var autofillBox = [];
var seassionStorageArray = [];
var allAsteroids = document.getElementById('allAsteroids');
var chosenAsteroid = document.getElementById('chosenAsteroid');
var sesionButton = document.getElementById('sesionAdd')

//Parsing JSON
function renderData(){
	var fromField = document.getElementById('from').value;
	var toField = document.getElementById('to').value;
	var ourReuqest = new XMLHttpRequest();
	ourReuqest.open('GET', 'https://api.nasa.gov/neo/rest/v1/feed?start_date=' + fromField + '&end_date=' + toField + '&api_key=x0HeIJzRCLm3lj0zrfXt2LltusKVCO7aoHmRkVq2');
	ourReuqest.onload = function(){
		if (ourReuqest.status >= 200 && ourReuqest.status < 400) {
			var fullRequest = JSON.parse(ourReuqest.responseText);
			renderElements(fullRequest);
		}else {
			alert("Server error, please try again")
		}
	}
	ourReuqest.send();
}
//Render html table 
function renderElements(data){
	var tableHtml = "";
	//Clear table of elements
	var clearTableElements = document.getElementsByClassName('table-row');
	while(clearTableElements[0]) {
	    clearTableElements[0].parentNode.removeChild(clearTableElements[0]);
	}
	autofillBox.length = 0;
	var asteroidsKey = Object.keys(data.near_earth_objects);
	for (var i = 0; i < asteroidsKey.length ; i++) {
	    for (var j = 0; j < data.near_earth_objects[asteroidsKey[i]].length && (i < 9); j++) {
	        if(data.near_earth_objects[asteroidsKey[i]][j].is_potentially_hazardous_asteroid == true){        
	            tableHtml += "<tr class='table-row'>" +
	            "<td>" + data.near_earth_objects[asteroidsKey[i]][j].close_approach_data[0].close_approach_date + "</td>" + 
	            "<td>" + data.near_earth_objects[asteroidsKey[i]][j].name + "</td>" +
	            "<td>" + data.near_earth_objects[asteroidsKey[i]][j].close_approach_data[0].relative_velocity.kilometers_per_hour + "</td>" + 
	            "<td>" + data.near_earth_objects[asteroidsKey[i]][j].estimated_diameter.meters.estimated_diameter_min + "</td>" + 
	            "<td>" + data.near_earth_objects[asteroidsKey[i]][j].estimated_diameter.meters.estimated_diameter_max + "</td>" +  
	            "</tr>"
	            autofillBox.push(data.near_earth_objects[asteroidsKey[i]][j]);
	        };
	    };
	};
	tableContainer.insertAdjacentHTML('beforeend',tableHtml);
	updateSelectedAsterodis();
	addAsteroid();
	showCOntent();
}
//Show Html after renderElements function is finish
function showCOntent(){
	var mainContent = document.getElementById('mainContent');
	mainContent.style.display='block';
}
//Adding elments to seassionStorageArray and removing from autofillBox arrays
allAsteroids.addEventListener('click', 
	function(e) {
	    if(e.target && e.target.nodeName == "LI") {
            var index = e.target.getAttribute('value');
		    seassionStorageArray.push(autofillBox[index]);
		    autofillBox.splice(index, 1);
		    addAsteroid();
		    updateSelectedAsterodis();
        }
});
//Adding elments to autofillBox and removing from seassionStorageArray arrays
chosenAsteroid.addEventListener('click', 
	function(e) {
	    var index = e.target.getAttribute('value');
	    autofillBox.push(seassionStorageArray[index]);
	    seassionStorageArray.splice(index, 1);
	    addAsteroid();
	    updateSelectedAsterodis();
});
//Render elemnts to Chose Asteroids box
function updateSelectedAsterodis() {
    var output = "";
    for (var i = 0; i < autofillBox.length; i++) {
        output += "<li href='#' value='" + i + "'>" + autofillBox[i].name + " " + "\n" + "</li>";
    };
    document.getElementById('allAsteroids').innerHTML = output;
};
//Render elemnts to Selected Asteroids box
function addAsteroid() {
    var output = "";
	if (seassionStorageArray.length == 0) {
    	var output = "<p>Please select asteroids</p>";
    	sesionButton.style.display= 'none';
	}else {
	    for (var i = 0; i < seassionStorageArray.length; i++) {
	        output += "<li href='#' value='" + i + "'>" + seassionStorageArray[i].name + " " + "\n" + "</li>";
	    };
	    sesionButton.style.display= 'block';
    }
    document.getElementById('chosenAsteroid').innerHTML = output;
};
// Adding elemetns to seassion storage
function addToLocalStorage(){
	var chosenAsteroids = JSON.stringify(seassionStorageArray);
	sessionStorage.setItem('seassionStorageArray', chosenAsteroids);
};
// Table sorting
var getCellValue = function(tr, idx){ 
	return tr.children[idx].innerText || tr.children[idx].textContent; 
}
var comparer = function(idx, asc) { 
	return function(a, b) { 
		return function(v1, v2) {
	        return v1 !== '' && v2 !== '' && !isNaN(v1) && !isNaN(v2) ? v1 - v2 : v1.
	        toString().localeCompare(v2);
    	}
    (getCellValue(asc ? a : b, idx), getCellValue(asc ? b : a, idx));
}};
Array.from(document.querySelectorAll('th')).forEach(function(th) { th.addEventListener('click', function() {
        var table = th.closest('table');
        Array.from(table.querySelectorAll('tr:nth-child(n+2)'))
        .sort(comparer(Array.from(th.parentNode.children).indexOf(th), this.asc = !this.asc)).
        forEach(function(tr) { table.appendChild(tr) });
    })
});